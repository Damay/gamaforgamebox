// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameForGameBox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GameForGameBox, "GameForGameBox" );
