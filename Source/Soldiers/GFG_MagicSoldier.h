﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Soldiers/GFG_SoldiersBase.h"
#include "GFG_MagicSoldier.generated.h"

/**
 * 
 */
UCLASS()
class GAMEFORGAMEBOX_API AGFG_MagicSoldier : public AGFG_SoldiersBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	//Ссылка созданого эффекта, чтобы его уничтожить
	UParticleSystemComponent* SpawnedParticleEmitter;

	//Переменная попадания трейса
	FHitResult Hit;

	//Функция эвента атаки
	virtual void AttackEvent() override;

	//Функция атаки
	UFUNCTION()
		virtual	void Attack(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointNotifyPayload);

	//Функция завершениея атаки
	UFUNCTION()
		virtual void AttackAnimEnd(UAnimMontage* Montage, bool bIsSuccess);

public:
	//Ссылка на эффект 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
	UParticleSystem* ParticleEmitter;
};
