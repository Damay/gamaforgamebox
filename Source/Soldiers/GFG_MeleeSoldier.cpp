﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Soldiers/GFG_MeleeSoldier.h"
#include "Castls/GFG_Castle.h"
#include "Kismet/GameplayStatics.h"


void AGFG_MeleeSoldier::AddedMoreParamsForOverlap(AActor* OverlapActor)
{
	//Инициализация вражеского солдата
	SoldierOverlaped = OverlapActor;

	//Проверка что враг является замком
	AGFG_Castle* Castl = Cast<AGFG_Castle>(OverlapActor);
	if (Castl)
	{
		//Установка флага на остановку перемещений
		OverlapCastl = true;
	}

	//Добавление таймера на атаку
	EnableTimerForAttack();
}

void AGFG_MeleeSoldier::BeginPlay()
{
	//Инициализация имени солдата
	SoldierName = "Melee";

	Super::BeginPlay();

	if (AttackMontage)
	{
		AnimAttackInstance = GetMesh()->GetAnimInstance();

		if (AnimAttackInstance)
		{
			//Добавление реакций на прохождение уведомления атаки в монтаже
			AnimAttackInstance->OnPlayMontageNotifyBegin.AddDynamic(this, &AGFG_MeleeSoldier::Attack);
		}
	}
}

void AGFG_MeleeSoldier::EnemyDead(AGFG_SoldiersBase* DeadEnemy)
{
	Super::EnemyDead(DeadEnemy);

	//Чистка таймера после убийства
	if(!OverlapCastl)
		GetWorldTimerManager().ClearTimer(TimerHandle_AttackTimer);
}

void AGFG_MeleeSoldier::AttackEvent()
{
	if (AttackMontage)
	{
		if (AnimAttackInstance)
		{
			//Проигрывание монтажа атаки
			AnimAttackInstance->Montage_Play(AttackMontage, AttackMontage->GetPlayLength());
		}
	}
}

void AGFG_MeleeSoldier::Attack(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointNotifyPayload)
{
	//Проверка имени уведомления
	if (NotifyName == "MeleeAttack")
	{
		//Проверка, что актор существует
		if (SoldierOverlaped)
		{
			//Нанесение урона
			const TSubclassOf<UDamageType> DamageType;
			UGameplayStatics::ApplyDamage(SoldierOverlaped, BaseDamage, GetController(), this, DamageType);
		}
	}
}


void AGFG_MeleeSoldier::EnableTimerForAttack()
{
	//Проверка что таймер уже идет
	if (TimerHandle_AttackTimer.IsValid())
	{
		
	}
	else
	{
		//Если нет, то нанесение первого удара и добавление таймера
		AttackEvent();
		GetWorldTimerManager().SetTimer(TimerHandle_AttackTimer, this, &AGFG_MeleeSoldier::AttackEvent, AttackDelay, true);

	}
}
