﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Soldiers/GFG_MagicSoldier.h"
#include "Castls/GFG_Castle.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

void AGFG_MagicSoldier::BeginPlay()
{
	//Инициализация имени солдата
	SoldierName = "Magic";

	Super::BeginPlay();

	//Инициализация таймера атаки
	GetWorldTimerManager().SetTimer(TimerHandle_AttackTimer, this, &AGFG_MagicSoldier::AttackEvent, AttackDelay, true);

	
	if (AttackMontage)
	{
		AnimAttackInstance = GetMesh()->GetAnimInstance();

		if (AnimAttackInstance)
		{
			//Добавление реакций на прохождение уведомления атаки в монтаже и окончания монтажа
			AnimAttackInstance->OnPlayMontageNotifyBegin.AddDynamic(this, &AGFG_MagicSoldier::Attack);
			AnimAttackInstance->OnMontageEnded.AddDynamic(this, &AGFG_MagicSoldier::AttackAnimEnd);
		}
	}

	//Инициализация первой атаки
	AttackEvent();
}

void AGFG_MagicSoldier::AttackEvent()
{
	//Получение всех солдат на уровне
	TArray<AActor*> ClassActor;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AGFG_SoldiersBase::StaticClass(), ClassActor);

	//Переменная, в которую будут записаны союзные солдаты
	TArray<AActor*> IgnoreActor;

	int i = 0;
	//Цикл по всем солдатам на уровне
	while (i < ClassActor.Num())
	{
		//Каст на базовый класс солдата
		AGFG_SoldiersBase* GetSoldierOfClass = Cast<AGFG_SoldiersBase>(ClassActor[i]);
		if (GetSoldierOfClass)
		{
			//Проверка что солдат союзный
			if (bIsEnemy == GetSoldierOfClass->bIsEnemy)
			{
				//Добавление
				IgnoreActor.Add(GetSoldierOfClass);
			}
		}
		i++;
	}

	//Отправка трейса от владельца по прямой до первого вражеского солдата или замка
	FCollisionQueryParams CollisionQueryParams;
	CollisionQueryParams.AddIgnoredActors(IgnoreActor);
	GetWorld()->LineTraceSingleByChannel(Hit, GetActorLocation(), FVector(GetActorLocation().X, (GetActorForwardVector().Y * 1000.0f), GetActorLocation().Z), ECollisionChannel::ECC_Visibility, CollisionQueryParams);


	if (AttackMontage)
	{
		if (AnimAttackInstance)
		{
			//Проигрывание монтажа атаки
			AnimAttackInstance->Montage_Play(AttackMontage, AttackMontage->GetPlayLength());
		}
	}
}

void AGFG_MagicSoldier::Attack(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointNotifyPayload)
{
	//Проверка имени уведомления
	if (NotifyName == "MagicAttack")
	{
		//Установка размера эфекта в зависимости от цели попадания
		FVector ScaleEmitter = FVector(0.3f, 0.3f, 0.3f);
		AGFG_Castle* Castle = Cast<AGFG_Castle>(Hit.GetActor());
		if (Castle)
		{
			ScaleEmitter = FVector(1.0f, 1.0f, 1.0f);
		}

		//Проверка, что актор не умер за время, прошедшее между трейсом и атакой
		if (Hit.GetActor())
		{
			//Создание эфекта на цели и нанесение урона
			SpawnedParticleEmitter = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),ParticleEmitter, Hit.GetActor()->GetActorLocation(), FRotator::ZeroRotator, ScaleEmitter);
			const TSubclassOf<UDamageType> DamageType;
			UGameplayStatics::ApplyDamage(Hit.GetActor(), BaseDamage, GetController(), this, DamageType);
		}
		else
		{
			//Если актор умер, то проведение атаки заново
			AttackEvent();
		}
	}
}

void AGFG_MagicSoldier::AttackAnimEnd(UAnimMontage* Montage, bool bIsSuccess)
{
	if (SpawnedParticleEmitter)
	{
		//Уничтожение эфекта после монтжа
		SpawnedParticleEmitter->DestroyComponent();
	}
}
