﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Soldiers/GFG_ArrowSoldier.h"
#include "Castls/GFG_Castle.h"
#include "Kismet/GameplayStatics.h"


void AGFG_ArrowSoldier::BeginPlay()
{
	//Инициализация имени солдата
	SoldierName = "Arrow";

	Super::BeginPlay();

	//Инициализация таймера атаки
	GetWorldTimerManager().SetTimer(TimerHandle_AttackTimer, this, &AGFG_ArrowSoldier::AttackEvent, AttackDelay, true);

	if (AttackMontage)
	{
		AnimAttackInstance = GetMesh()->GetAnimInstance();

		if (AnimAttackInstance)
		{
			//Добавление реакций на прохождение уведомления атаки в монтаже и окончания монтажа
			AnimAttackInstance->OnPlayMontageNotifyBegin.AddDynamic(this, &AGFG_ArrowSoldier::Attack);
			AnimAttackInstance->OnMontageEnded.AddDynamic(this, &AGFG_ArrowSoldier::AttackAnimEnd);
		}
	}

	//Инициализация первой атаки
	AttackEvent();
}

void AGFG_ArrowSoldier::AddedMoreParamsForOverlap(AActor* OverlapActor)
{
	//Установка флага, запрещающего пермещеие
	Overlaped = true;
}

void AGFG_ArrowSoldier::AttackEvent()
{
	//Остановка перемещения
	CanMove = false;

	if (AttackMontage)
	{
		if (AnimAttackInstance)
		{
			//Проигрывание монтажа атаки
			AnimAttackInstance->Montage_Play(AttackMontage, AttackMontage->GetPlayLength());
		}
	}
}

void AGFG_ArrowSoldier::Attack(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointNotifyPayload)
{
	//Проверка имени уведомления
	if (NotifyName == "ArrowAttack")
	{
		FRotator SpawnRotation;

		//Установка ротации стрелы, в зависимоти от принадлежности к стороне конфликта
		if (bIsEnemy)
			SpawnRotation = FRotator(0.0f, 0.0f, 180.0f);
		else
			SpawnRotation = FRotator(0.0f, 0.0f, 0.0f);

		//Создание стрелы с передачей начальных параметров
		AGFG_Arrow* Arrow = Cast<AGFG_Arrow>(GetWorld()->SpawnActorDeferred<AGFG_Arrow>(ArrowInstance, FTransform(SpawnRotation, GetActorLocation()), nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn));
		if (Arrow)
		{
			Arrow->bIsEnemy = bIsEnemy;
			if (bIsEnemy)
				Arrow->Speed = ArrowSpeed * -1;
			else
				Arrow->Speed = ArrowSpeed;

			Arrow->BaseDamage = BaseDamage;
			UGameplayStatics::FinishSpawningActor(Arrow, FTransform(SpawnRotation, GetActorLocation()));
		}
	}
}

void AGFG_ArrowSoldier::AttackAnimEnd(UAnimMontage* Montage, bool bIsSuccess)
{
	//Запрет возобновления перемещения, если солдат соприкасается с врагом
	if (!Overlaped)
	{
		CanMove = true;
	}
}

void AGFG_ArrowSoldier::EnemyDead(AGFG_SoldiersBase* DeadEnemy)
{
	Super::EnemyDead(DeadEnemy);

	//Снятие флага на запрет перемещения
	Overlaped = false;
}
