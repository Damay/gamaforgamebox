﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Soldiers/GFG_SoldiersBase.h"
#include "GFG_Arrow.h"
#include "GFG_ArrowSoldier.generated.h"

/**
 * 
 */
UCLASS()
class GAMEFORGAMEBOX_API AGFG_ArrowSoldier : public AGFG_SoldiersBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	//Флаг достижения противника, чтобы убрать ошибочное продвижение 
	bool Overlaped = false;

	//Функция добавления специфических, для класса, параметров
	virtual void AddedMoreParamsForOverlap(AActor* OverlapActor) override;

	//Функция убийства врага
	virtual	void EnemyDead(AGFG_SoldiersBase* DeadEnemy) override;

	//Функция эвента атаки
	virtual void AttackEvent() override;

	//Функция атаки
	UFUNCTION()
		virtual	void Attack(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointNotifyPayload);

	//Функция завершениея атаки
	UFUNCTION()
		virtual void AttackAnimEnd(UAnimMontage* Montage, bool bIsSuccess);

public:

	//Переменная скорости полета стрелы. Для удобства, устанавливается в блюпринте
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Arrow")
		float ArrowSpeed = 30.0f;

	//Переменная самой стрелы. Для удобства, устанавливается в блюпринте
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Arrow")
		TSubclassOf<AGFG_Arrow> ArrowInstance;


};
