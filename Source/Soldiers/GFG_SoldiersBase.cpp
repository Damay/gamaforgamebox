﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "GFG_SoldiersBase.h"
#include "Game/GFG_GameInstance.h"
#include "FuncLibrary/Type.h"
#include "Castls/GFG_Castle.h"
#include "GameFramework/CharacterMovementComponent.h"


// Sets default values
AGFG_SoldiersBase::AGFG_SoldiersBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Создание сферы соприкосновение с врагом
	SphereCollisionForAttack = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollisionForAttack"));
    if (SphereCollisionForAttack)
    {
    	SphereCollisionForAttack->InitSphereRadius(32.0f);
		SphereCollisionForAttack->SetRelativeLocation(FVector(10.0f, 0.0f, 30.f));
		SphereCollisionForAttack->SetCollisionProfileName("OverlapAll");
		SphereCollisionForAttack->SetupAttachment(RootComponent);
    }

	//Создание компонента жизни
	HealthComponent = CreateDefaultSubobject<UGFG_HealthComponent>(TEXT("HealthComponent"));
}

// Called when the game starts or when spawned
void AGFG_SoldiersBase::BeginPlay()
{
	Super::BeginPlay();

	//Добавление реакций на соприкосновение
	if (SphereCollisionForAttack)
	{
		SphereCollisionForAttack->OnComponentBeginOverlap.AddDynamic(this, &AGFG_SoldiersBase::OnSphereBeginOverlap);
	}

	//Добавление реакций на смерть
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &AGFG_SoldiersBase::OnDead);
	}

	//Инициализация параметров из таблицы
	UGFG_GameInstance* myGI = Cast<UGFG_GameInstance>(GetGameInstance()); FSoldierCharacter SoldierCharacter;
	if (myGI)
	{
		myGI->GetSoldierInfoByName(SoldierName, SoldierCharacter);

		GetCharacterMovement()->MaxWalkSpeed = SoldierCharacter.Speed;
		BaseDamage = SoldierCharacter.Damage;
		AttackDelay = SoldierCharacter.AttackDelay;
		AttackMontage = SoldierCharacter.AttackMontage;
	}
}

void AGFG_SoldiersBase::AddedMoreParamsForOverlap(AActor* OverlapActor)
{
}

// Called every frame
void AGFG_SoldiersBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Передвижение если оно возможно
	if(CanMove)
	{
		AddMovementInput(GetActorForwardVector());
	}
}

// Called to bind functionality to input
void AGFG_SoldiersBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AGFG_SoldiersBase::EnemyDead(AGFG_SoldiersBase* DeadEnemy)
{
	//Возобновление передвижения
	CanMove = true;

	//Удаление из врагов
	OverlapSoldiers.Remove(DeadEnemy);
}

void AGFG_SoldiersBase::OnDead()
{
	//Сообщение всем, кто соприкасается о том, что солдат умирает
	int i = 0;
	while (i <= OverlapSoldiers.Num())
	{
		if(OverlapSoldiers.IsValidIndex(i))
		{
			OverlapSoldiers[i]->EnemyDead(this);
		}
		i++;
	}

	//Уничтожение
	Destroy();
}

float AGFG_SoldiersBase::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	//Изменение жизни при нанесении урона
	HealthComponent->ChangeHealthValue(DamageAmount * (-1));
	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

void AGFG_SoldiersBase::OnSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
                                             UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AGFG_SoldiersBase* EnemyActor = Cast<AGFG_SoldiersBase>(OtherActor);
	AGFG_Castle* Castle = Cast<AGFG_Castle>(OtherActor);

	//Проверки пересекаемых объектов
	if (EnemyActor)
	{
		//Проверка есловия что это враг
		if (bIsEnemy != EnemyActor->bIsEnemy)
		{
			//Добавление в список соприкасаемых врагов
			OverlapSoldiers.Add(EnemyActor);

			CanMove = false;
			AddedMoreParamsForOverlap(EnemyActor);
		}
	}
	else
	{
		if (Castle)
		{
			//Проверка есловия что это враг
			if (bIsEnemy != Castle->bIsEnemy)
			{
				CanMove = false;
				AddedMoreParamsForOverlap(Castle);
			}
		}
	}
}

void AGFG_SoldiersBase::AttackEvent()
{
}

