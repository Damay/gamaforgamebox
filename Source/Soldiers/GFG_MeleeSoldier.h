﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Soldiers/GFG_SoldiersBase.h"
#include "GFG_MeleeSoldier.generated.h"

/**
 * 
 */
UCLASS()
class GAMEFORGAMEBOX_API AGFG_MeleeSoldier : public AGFG_SoldiersBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	// Переменная актора, которому будет наносится урон
	AActor* SoldierOverlaped;

	//Флаг достижения замка, чтобы убрать ошибочное продвижение 
	bool OverlapCastl = false;

	//Функция добавления специфических, для класса, параметров
	virtual void AddedMoreParamsForOverlap(AActor* OverlapActor) override;

	//Функция убийства врага
	virtual	void EnemyDead(AGFG_SoldiersBase* DeadEnemy) override;

	//Функция эвента атаки
	virtual void AttackEvent() override;

	//Функция таймера
	void EnableTimerForAttack();

	//Функция атаки
	UFUNCTION()
		virtual void Attack(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointNotifyPayload);
};