﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Component/GFG_HealthComponent.h"
#include "Components/SphereComponent.h"
#include "GFG_SoldiersBase.generated.h"

UCLASS()
class GAMEFORGAMEBOX_API AGFG_SoldiersBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGFG_SoldiersBase();


	FORCEINLINE class UGFG_HealthComponent* GetHealthComponent() const { return HealthComponent; }

	FORCEINLINE class USphereComponent* GetSphereCollisionComponent() const { return SphereCollisionForAttack; }

private:

	//Добавление компонента жизни
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
		class UGFG_HealthComponent* HealthComponent;

	//Добавление сферы на соприкосновение с врагом
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* SphereCollisionForAttack;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Функция добавления специфических, для классов, параметров
	virtual void AddedMoreParamsForOverlap(AActor* OverlapActor);

	//Переменая имени солдата
	FName SoldierName;

	//Переменная задержки между атаками
	float AttackDelay = 0.0f;

	//Ссылка на анимацию атаки
	UAnimMontage* AttackMontage = nullptr;

	//Сслыка на всех врагов, с которыми столкнулся солдат
	TArray<AGFG_SoldiersBase*> OverlapSoldiers;

	//Переменная экземпляра анимации
	UAnimInstance* AnimAttackInstance;

	//Переменная таймера атаки
	FTimerHandle TimerHandle_AttackTimer;

	//Переменная наносимого урона
	float BaseDamage = 0.0f;

	//Флаг возможности передвижения
	bool CanMove = true;

	//Функция убийства врага
	UFUNCTION()
		virtual	void EnemyDead(AGFG_SoldiersBase* DeadEnemy);

	//Функция Смерти
	UFUNCTION()
		void OnDead();

	//Функция получения урона
	UFUNCTION()
		virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	//Столкновение с объектом
	UFUNCTION()
		virtual void OnSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	//Функция эвента атаки
	UFUNCTION()
		virtual void AttackEvent();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Переменная принадлежности к стороне конфликта
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="SettingSoldier")
		bool bIsEnemy = false;
};
