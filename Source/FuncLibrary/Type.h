﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Soldiers/GFG_SoldiersBase.h"
#include "Type.generated.h"

USTRUCT(BlueprintType)
struct FSoldierCharacter : public FTableRowBase
{
	GENERATED_BODY()

	//Имя солдата
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Soldier")
		FName Name;
	//Скорость перемещения
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Soldier")
		float Speed = 0.0f;
	//Урон
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Soldier")
		float Damage = 0.0f;
	//Объект солдата
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Soldier")
		TSubclassOf<class AGFG_SoldiersBase> Soldier = nullptr;
	//Задержка между ударами
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Soldier")
		float AttackDelay = 0.0f;
	//Монтаж атаки
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Soldier")
		UAnimMontage* AttackMontage = nullptr;
};
/**
 * 
 */
UCLASS()
class GAMEFORGAMEBOX_API UType : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
