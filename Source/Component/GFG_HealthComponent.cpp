﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GFG_HealthComponent.h"

// Sets default values for this component's properties
UGFG_HealthComponent::UGFG_HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// ...
}


// Called when the game starts
void UGFG_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UGFG_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}
	

float UGFG_HealthComponent::GetCurrentHealth()
{
	return Health;
}

void UGFG_HealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UGFG_HealthComponent::ChangeHealthValue(float ChangeValue)
{
	//Проверка жизни
	if (bIsAlive == true)
	{
		//Проверка урона или исцеления
		if (ChangeValue < 0.0f)
			Health += ChangeValue * CoefDamage;
		else
			Health += ChangeValue * CoefHealing;

		//Сигнал изменения жизни
		OnHealthChange.Broadcast(Health, ChangeValue);

		//Проверка смерти и слишком большого значения здоровья
		if (Health > 100.0f)
		{
			Health = 100.0f;
		}
		else
		{
			if (Health <= 0.0f)
			{
				OnDead.Broadcast();
				bIsAlive = false;
			}
		}
	}
}

