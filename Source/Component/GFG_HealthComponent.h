﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GFG_HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

USTRUCT(BlueprintType)
struct FStartParam
{
	GENERATED_BODY()

};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMEFORGAMEBOX_API UGFG_HealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGFG_HealthComponent();

	//Дерегат изменения здоровья (для расширения)
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnHealthChange OnHealthChange;
	//Дерегат смерти
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnDead OnDead;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	//Жизни
	float Health = 100.0f;

public:
	//Коэфициент урона
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float CoefDamage = 1.0f;
	//Коэфициент исцеления (для расширения)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float CoefHealing = 1.0f;
	//Флаг жизни
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
		bool bIsAlive = true;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Фунция получения величины здоровья
	UFUNCTION(BlueprintCallable, Category = "Health")
		float GetCurrentHealth();
	//Фунция установки величины здоровья (для расширения)
	UFUNCTION(BlueprintCallable, Category = "Health")
		void SetCurrentHealth(float NewHealth);
	//Изменение величины здоровья
	UFUNCTION(BlueprintCallable, Category = "Health")
		virtual void ChangeHealthValue(float ChangeValue);

		
};
