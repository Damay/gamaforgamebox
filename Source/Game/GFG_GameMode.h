﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Soldiers/GFG_SoldiersBase.h"
#include "GFG_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class GAMEFORGAMEBOX_API AGFG_GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	//Функция создания солдат на поле боя
	UFUNCTION(BlueprintCallable)
		void SpawnSoldier(bool bIsEnemy, TSubclassOf<AGFG_SoldiersBase> CharacterClass);
};
