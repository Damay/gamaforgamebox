﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/GFG_GameMode.h"
#include "Kismet/GameplayStatics.h"

void AGFG_GameMode::SpawnSoldier(bool bIsEnemy, TSubclassOf<AGFG_SoldiersBase> CharacterClass)
{
	FVector SpawnLocation;
	FRotator SpawnRotation;

	//Установка параметров создания и поворота солдата, в зависимоти от принадлежности к стороне конфликта
	if (bIsEnemy)
	{
		SpawnRotation = FRotator(0.0f, -90.0f, 0.0f);
		SpawnLocation = FVector(150.0f, 200.0f, 27.0f);
	}
	else
	{
		SpawnRotation = FRotator(0.0f, 90.0f, 0.0f );
		SpawnLocation = FVector(150.0f, -125.0f, 27.0f);
	}

	//Создание солдата на поле боя
	AGFG_SoldiersBase* Soldier = Cast<AGFG_SoldiersBase>(GetWorld()->SpawnActorDeferred<AGFG_SoldiersBase>(CharacterClass, FTransform(SpawnRotation, SpawnLocation), nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn));
	if (Soldier)
	{
		Soldier->bIsEnemy = bIsEnemy;
		
		UGameplayStatics::FinishSpawningActor(Soldier, FTransform(SpawnRotation, SpawnLocation));
	}
}
