﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FuncLibrary/Type.h"
#include "Engine/DataTable.h"
#include "GFG_GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class GAMEFORGAMEBOX_API UGFG_GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	//Добавление таблицы параметров солдат
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
		UDataTable* SoldierInfoTable = nullptr;
	//Поиск параметров по имени
	UFUNCTION(BlueprintCallable)
		bool GetSoldierInfoByName(FName NameSoldier, FSoldierCharacter& SoldierCharacter);
};
