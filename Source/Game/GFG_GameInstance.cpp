// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/GFG_GameInstance.h"

bool UGFG_GameInstance::GetSoldierInfoByName(FName NameSoldier, FSoldierCharacter& SoldierCharacter)
{
	bool bIsFind = false;

	if (SoldierInfoTable)
	{
		FSoldierCharacter* EnemyInfoRow = SoldierInfoTable->FindRow<FSoldierCharacter>(NameSoldier, "", false);

		if (EnemyInfoRow)
		{
			bIsFind = true;
			SoldierCharacter = *EnemyInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("USnowBall_TPSGameInstance::GetWeaponInfoByName - WeaponTable is NULL"))
	}

	return bIsFind;
}
