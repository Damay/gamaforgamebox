// Fill out your copyright notice in the Description page of Project Settings.


#include "GFG_Arrow.h"
#include "Castls/GFG_Castle.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Soldiers/GFG_SoldiersBase.h"

// Sets default values
AGFG_Arrow::AGFG_Arrow()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
    if (Mesh)
    {
	    	RootComponent = Mesh;
    }

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
    if (SphereCollision)
    {
		SphereCollision->InitSphereRadius(6.0f);
		SphereCollision->SetCollisionProfileName("OverlapAll");
		SphereCollision->SetupAttachment(RootComponent);
    }

	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
    if (Arrow)
    {
		Arrow->SetupAttachment(RootComponent);
		Arrow->SetRelativeRotation(FQuat(FRotator(0.0f, 90.0f, 0.0f)));
    }
}

// Called when the game starts or when spawned
void AGFG_Arrow::BeginPlay()
{
	Super::BeginPlay();

	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AGFG_Arrow::OnSphereBeginOverlap);

}

void AGFG_Arrow::Attack(AActor* SoldierOverlaped)
{
	if (SoldierOverlaped)
	{
		const TSubclassOf<UDamageType> DamageType;
		UGameplayStatics::ApplyDamage(SoldierOverlaped, BaseDamage, UGameplayStatics::GetPlayerController(GetWorld(), 0), this, DamageType);
	}

	Destroy();
}

// Called every frame
void AGFG_Arrow::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector MoveVector;
	MoveVector.X = GetActorLocation().X;
	MoveVector.Z = GetActorLocation().Z;
	MoveVector.Y = DeltaTime * Speed + Arrow->GetForwardVector().Y + GetActorLocation().Y;

	SetActorLocation(MoveVector);
}

void AGFG_Arrow::OnSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AGFG_SoldiersBase* EnemyActor = Cast<AGFG_SoldiersBase>(OtherActor);
	if (EnemyActor)
	{

		if (bIsEnemy != EnemyActor->bIsEnemy)
		{
			Attack(EnemyActor);
		}
	}
	else
	{
		AGFG_Castle* Castle = Cast<AGFG_Castle>(OtherActor);
		if (Castle)
		{
			if (bIsEnemy != Castle->bIsEnemy)
			{
				Attack(Castle);
			}
		}
	}
}

