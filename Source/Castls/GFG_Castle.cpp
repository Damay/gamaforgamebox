﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GFG_Castle.h"

// Sets default values
AGFG_Castle::AGFG_Castle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UGFG_HealthComponent>(TEXT("HealthComponent"));
}

// Called when the game starts or when spawned
void AGFG_Castle::BeginPlay()
{
	Super::BeginPlay();
}

float AGFG_Castle::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	//Изменение жизни при нанесении урона
	HealthComponent->ChangeHealthValue(DamageAmount * (-1));

	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

// Called every frame
void AGFG_Castle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

