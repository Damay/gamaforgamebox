﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Component/GFG_HealthComponent.h"
#include "GFG_Castle.generated.h"

UCLASS()
class GAMEFORGAMEBOX_API AGFG_Castle : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGFG_Castle();

	FORCEINLINE class UGFG_HealthComponent* GetHealthComponent() const { return HealthComponent; }

private:

	//Добавление компонента жизни
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
		class UGFG_HealthComponent* HealthComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Функция получения урона
	UFUNCTION()
		virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
public:


	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Переменная принадлежности к стороне конфликта
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SettingCastl")
	bool bIsEnemy = false;
};
